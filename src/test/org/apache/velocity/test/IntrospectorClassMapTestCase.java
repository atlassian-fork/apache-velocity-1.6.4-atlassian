package org.apache.velocity.test;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.log.Log;
import org.apache.velocity.util.introspection.ClassMap;
import org.apache.velocity.util.introspection.Introspector;
import org.apache.velocity.util.introspection.IntrospectorCache;
import org.apache.velocity.util.introspection.MethodMap;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Test case for the Velocity Introspector test if introspector cache class is initialised properly
 * through property.
 */
public class IntrospectorClassMapTestCase extends BaseTestCase
{
    public static final String EXPECTED_METHOD_NAME = "properName";
    public static final Object[] EXPECTED_PARAMS = new Object[] { };
    private static final Method expectedMethod;

    static {
        try
        {
            expectedMethod = TestClassMap.class.getMethod("testMethod", new Class[] { });
        }
        catch (NoSuchMethodException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates a new instance.
     */
    public IntrospectorClassMapTestCase(String name)
    {
        super(name);
    }

    /**
     * Get the containing <code>TestSuite</code>.  This is always <code>VelocityTestSuite</code>.
     *
     * @return The <code>TestSuite</code> to run.
     */
    public static Test suite()
    {
        return new TestSuite(IntrospectorClassMapTestCase.class);
    }

    public void setUp() throws Exception
    {
        super.setUp();

    }

    public void testIntrospectorInitializesCacheFromProperties()
            throws Exception
    {
        RuntimeSingleton.getConfiguration().addProperty(RuntimeConstants.INTROSPECTOR_CACHE_CLASS, TestIntrospectorCache.class.getName());
        final TestIntrospector introspector = new TestIntrospector();
        final Method method = introspector.getMethod(
                TestClassMap.class, EXPECTED_METHOD_NAME, EXPECTED_PARAMS);


        assertEquals(method, expectedMethod);
        final IntrospectorCache introspectorCache = introspector.getIntrospectorCache();
        assertEquals("Unexpected class for getIntrospectorCache ", introspectorCache.getClass(), TestIntrospectorCache.class);
        final TestIntrospectorCache testIntrospectorCache = (TestIntrospectorCache) introspectorCache;
        assertNotNull("Logger should be initialized", testIntrospectorCache.getLog());
        assertEquals("There should be exactly one class introspected by the cache", 1, testIntrospectorCache.getClassesCache().size());
        assertNotNull("IntrospectorCache should contain entry for TestClassMap", testIntrospectorCache.getClassesCache().get(TestClassMap.class));

    }


    public static class TestIntrospectorCache implements IntrospectorCache
    {
        private final Log log;
        private final Map<Class<?>, ClassMap> classesCache = new HashMap<Class<?>, ClassMap>();

        public TestIntrospectorCache(Log log)
        {
            this.log = log;
        }

        public Map<Class<?>, ClassMap> getClassesCache()
        {
            return classesCache;
        }

        public Log getLog()
        {
            return log;
        }

        @Override
        public void clear()
        {
        }

        @Override
        public ClassMap get(final Class c)
        {
            return classesCache.get(c);
        }

        @Override
        public ClassMap put(final Class c)
        {
            classesCache.put(c, new TestClassMap());
            return classesCache.get(c);
        }
    }

    private static class TestClassMap implements ClassMap
    {
        @Override
        public Method findMethod(final String name, final Object[] params) throws MethodMap.AmbiguousException
        {
            if (EXPECTED_METHOD_NAME.equals(name) && Arrays.equals(EXPECTED_PARAMS, params))
            {
                return expectedMethod;
            }
            else
            {
                return null;
            }
        }

        public void testMethod()
        {

        }
    }

    private static class TestIntrospector extends Introspector
    {
        public TestIntrospector() {super(RuntimeSingleton.getLog(), RuntimeSingleton.getRuntimeServices());}

        @Override
        public IntrospectorCache getIntrospectorCache()
        {
            return super.getIntrospectorCache();
        }
    }
}
